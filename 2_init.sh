#!/bin/bash

# necessory tools for daily work, (but not necessory if you just want to play within virtual pc)

function pacman () {
    command pacman --noconfirm "$@"
}

function yay () {
    command yay --noconfirm "$@";
}

set -xeu

# pacman -S chromium
yay -S google-chrome microsoft-edge-stable

pacman -S mosh

# network tools
pacman -S wireshark-cli

# tig 是 git 的命令行工具, 另一个是 smartgit
pacman -S tig

pacman -S smartmontools

# 安装多媒体相关的解码库及 H.264 解码支持，gst-libav 真的必须的吗？试试。

# Emacs telega 客户端用 telegram-tdlib
# wechat-universal-bwrap 需要加新的源
pacman -S telegram-desktop
yay -S linuxqq wechat-universal-bwrap

# 好几个软件，例如，mu 依赖 xapian-core, mu 也依赖于 gmime3, 编译 mu 依赖 meson.
pacman -S xapian-core meson gmime3

# 检测有哪些软件坏了，运行 check-broken-packages
# yay -S check-broken-packages-pacman-hook-git

# 可选的安装 git-annex, 会拉入 haskell 作为依赖
# pacman -S git-annex

# ruby2d 依赖
# sudo pacman -S sdl2 sdl2_image sdl2_mixer sdl2_ttf
# sudo pacman -S glew glm freetype2

# pacman -S plasma kde-applications
