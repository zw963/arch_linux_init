# 重要的话写在最前面

# 1. 有一次电脑重装系统时, refind 总是无法找到我的 Linux, 折腾了一天, 才发现是
# refind 版本和驱动版本不一致的问题.
# 2. ext4 分区格式也是需要驱动的, 这点还是挺让人意外的.
# 3. 之前 Zbook 15 G2 里面安装了 3 块硬盘，refind 安装在  m.2 接口的硬盘上，
#    但是 refind 死活找不到安装在 sata 硬盘驱动器上的的 Linux root 分区。
#    百思不得其解，最终发现问题出现在 Zbook 15 G2 的 FastBoot, 打开这个选项将限制
#    仅仅从 internel HDD 以及 PXE 启动（其实没有限制，你还是可以 F9 手动选择从其他硬盘启动）
#    只是默认情况下，总是从 m.2 硬盘启动，并且，refind 无法发现其他硬盘上的分区。

# =============== 安装步骤 ===============

# 之前安装过很多次 refind, 但是都是在新电脑买下以后, 首先安装 Windows 10, 然后
# Windows 10 会自动创建 esp 分区, 此时的做法是, 将 Windows 生成的文件换个名, 备份一份,
# 然后使用 refind 替换原始文件, 增加 refind.conf 配置文件, 编写进入 Linux 的入口配置,
# 并且, 添加一个指向 Win10 原始文件的入口.

# 而下面的安装步骤, 完全不依赖于首先安装 Windows.

# 安装步骤: (即使没有 Window, 也可以成功)

# 使用 gparted 创建一个 fat32/fat16 分区, 对这个分区做如下标记:
# 1.  name 设定为 "EFI System Partition", 似乎设定 label esp 是非必要的
# 2.  开启如下标记: boot, esp, no_automount
#    上面的步骤应该是非必需的, 但是考虑完整性, 写入.
# 3. linux 内核文件所在分区的 label 为, 例如: ArchLinux, 这一步必须和下面的配置中的 volume 以及 LABEL=?? 相对应.

# 使用 arch 启动盘启动电脑, 假设那个分区是: /dev/sda1 运行如下命令:

# $: refind-insall --usedefault /dev/sda1 --alldrivers, 这将在该分区创建如下目录结构:

# EFI/BOOT/{refind.conf,bootx64.efi,icons/,drivers_x64,keys} 等

# 最后一步, 拷贝 refind.conf, 并按照所需进行编辑, 下面是一个例子:

# timeout 20
# scanfor external,optical,manual

# menuentry "Arch Linux intel-ucode" {
#     icon     /EFI/BOOT/icons/os_arch.png
#     volume   "ArchLinux"
#     loader   /boot/vmlinuz-linux
#     initrd   /boot/initramfs-linux.img
#     options "root=LABEL=ArchLinux rw initrd=boot\intel-ucode.img"
# }

# 这里需要提及的一个小坑是, 安装 linux, linux-headers 的时候, 必须在 Arch 安装盘
# 上通过 arch-chroot 的方式来进行, 确保内核安装的 hook 有正确执行, 否则,
# /boot/initramfs-linux.img 可能是不存在的.

# 大功告成

# 有关修改引导顺序的问题。
# 如果进不了系统，可以尝试以下办法：
# 1. 对于较新一点的硬件，可以直接尝试 refind boot cd, 里面有一个工具用来修改 boot 顺序。
# 2. 有一些电脑上（例如 HP), 当按下 F9, 调出启动菜单时，它有一个选项叫做： "Boot from EFI file"
#    你可以一步一步选择，找到 refind 启动文件（bootx64.efi), 就可以启动了。
# 3. 如果可以启动系统，运行 efibootmgr -v 查看顺序。然后复制原来的顺序
#    evibootmgr -o 新的顺序 即可。但是我发现我的 zbook 15 G2 更新顺序重启后又恢复了，不知道为啥。（FAST boot?)
# 终极办法： 删除所有条目，并重新加入一个新的条目。
# 例如： efibootmgr -b 0000 -B 这会删除 0000 条目，使用此方法，删除所有条目。
# efibootmgr -c -d /dev/sda -p 1 -l \EFI\BOOT\bootx64.efi 重新新增一个条目
