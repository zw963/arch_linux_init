#!/bin/bash

# ------------------------------ For AMD GPU ------------------------------

#  DRI driver for 3D acceleration.
pacman -S mesa lib32-mesa

# 2D acceleration in Xorg
pacman -S xf86-video-amdgpu

pacman -S amdvlk lib32-amdvlk libva-utils

# AMD 专有驱动
pacman -S vulkan-amdgpu-pro lib32-vulkan-amdgpu-pro

# 上面驱动的另一选择, 如果上面的驱动有问题, 尝试下面的
# 经过较长时间的测试，vulkan-radeon 开源驱动，有时候会让显示器黑屏，现在在测试 amdvlk 驱动
# pacman -S vulkan-radeon lib32-vulkan-radeon

# Support for accelerated video decoding
pacman -S libva-mesa-driver lib32-libva-mesa-driver

# 查看 GPU 运行状态
pacman -S radeontop nvtop

# python-pytorch-rocm for to use PyTorch with ROCm
pacman -S rocm-hip-sdk rocm-opencl-sdk python-pytorch-rocm rocm-core

pacman -S ollama-rocm

pacman -S python-pytorch-rocm

pacman -S steam
