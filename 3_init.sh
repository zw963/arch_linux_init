#!/bin/bash

function pacman () {
    command pacman --noconfirm "$@"
}

function yay () {
    command yay --noconfirm "$@";
}

set -xeu

# For compile firefox
pacman -S fakeroot xorg-Xauth xorg-server-xvfb mercurial

# for programming tools

# arch build packages tools
pacman -S devtools

# pg not split to server and client.
pacman -S postgresql dbeaver
sudo chown postgres:postgres /var/lib/postgres/data/ # 非必需, 安装程序会设置正确权限, 不过保险点再执行一次.
sudo -u postgres initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'
systemctl enable postgresql
systemctl start postgresql
sudo -u postgres createuser `whoami` -s
pacman -S jre-openjdk # 可能 dbeaver 需要

pacman -S android-tools

pacman -S ninjama

pacman -S gdb libtree

#wasm

pacman -S binaryen wasm-pack wasmer wasmtime

# strace -p PID, 可以用来观测 application 的系统调用
pacman -S graphviz kcachegrind valgrind pref hotspot strace

# library
# archlinux-java status 查看当前安装的 java 版本
# sudo archlinux-java set 设置
# jre11-openjdk 是因为 charles 用到。(注意，有些包需要 openjdk, 而不是 headless, 这是一个坑)
pacman -S java-runtime-headless jre11-openjdk

pacman -S android-studio genymotion

pacman -S haskell-language-server

# 需安装的插件：flutter, dart, flutter intl, i18n arb editor, chinese translation.
pacman -S code
yay -S code-marketplace code-features

# For Crystal
# sudo pacman -S crystal shards

# sudo pacman -S erlang elixir

pamcan -S scrcpy

function install_virtualbox () {
    # 如果使用默认内核，用 virtualbox-host-modules-arch
    # 但是如果安装多内核，或可能切换到其他内核，用 virtualbox-host-dkms
    # 感觉使用 virtualbox-host-dkms，除了升级内核的时候，需要花费较多时间升级 virtualbox 的内核模块。
    # 也没有其他什么问题。
    pacman -S virtualbox virtualbox-host-dkms virtualbox-guest-iso virtualbox-ext-oracle
    sudo gpasswd -a zw963 vboxusers
    sudo modprobe vboxdrv vboxnetadp vboxnetflt
}

function install_vmware () {
    # vmware licenses https://gist.github.com/williamgh2019/cc2ad94cc18cb930a0aab42ed8d39e6f

    pacman -S vmware-workstation

    # VMWARE 网络访问
    systemctl enable vmware-networks.service
    systemctl start vmware-networks.service

    # VMWARE USB 共享
    systemctl enable vmware-usbarbitrator.service
    systemctl start vmware-usbarbitrator.service

    # VMWARE 目录共享
    # systemctl enable vmware-hostd.service
    # systemctl start vmware-hostd.service

}

function install_raspberry () {
    pacman -S qemu-user-static binfmt-qemu-static
    systemctl restart systemd-binfmt.service
    docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    docker run --rm -t arm64v8/ubuntu uname -m
    # check ARM architecture is supported
    # ls /proc/sys/fs/binfmt_misc

    # Instead of mounting an SD card, we use an image from Arch Linux ARM:

    #     Download an image file from Arch Linu ARM

    #     Create a folder that you want to use as root for the chroot environment.

    #     Extract the image files from the archive into that folder:

    #      $ bsdtar -xpf <image-archive> -C <root-folder>

    #     Make the new root folder a mount point:

    #      $ mount --bind <root-folder> <root-folder>

    #     Chroot into the new folder, initialize the pacman keyring and populate the Arch Linux ARM package signing keys:

    #      $ arch-chroot <root-folder>
    #      $ pacman-key --init
    #      $ pacman-key --populate archlinuxarm

    #     Update the chroot environment and use it:

    #      $ pacman -Syu

    # The AUR package armutils contains the tool mkarmroot which implements this method.
}
