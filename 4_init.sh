#!/bin/bash

function pacman () {
    command pacman --noconfirm "$@"
}

function yay () {
    command yay --noconfirm "$@";
}

set -xeu
# for video/picture, wps tools

pacman -S gimp gimp-help-zh_cn

pacman -S obs-studio
# 使用 obs 在 Wayland 下实现简单的录屏：
# 1. 安装必需的包
# 2. 复制 /usr/share/applications/com.obsproject.Studio.desktop 到 ~/.local/share/applications/
#    然后将 Exec=obs 替换为 env QT_QPA_PLATFORM=wayland obs
# 3. 运行 obs, 然后选择要录屏的屏幕，此时应该可以看到屏幕预览了。
# 4. 在来源面板里面选择 PipeWire, 如果看不到，检查以上步骤。
# 5. 如果提示 NVENC 错误，选择 文件 -> 设置 -> 输出 -> 编码器 -> 软件(x264)
#    如果你有 vaapi，也可以去高级选项里用上
# 6. 如果不需要声音，在混音器面板关闭它
# 7. 点击 “开始录制” 开始录屏，“停止录制” 停止录屏

# 注意，如果使用 GNOME, 事实上查询快捷键的 screenshots 分组里，
# 可以看到，已经内置这个功能了，Ctrl+Alt+Shift+r

pacman -S libreoffice-fresh
yay -S wps-office-cn ttf-wps-fonts
yay -S xunlei-bin

pacman -S samba
yay -S wsdd2                # Support Win 10 to see current samba driver.
systemctl enable smb nmb wsdd2

# gnome 扩展

# Freon
